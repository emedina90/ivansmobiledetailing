<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('services', 'MainController@services');
Route::get('about-us', 'MainController@aboutus');
Route::get('booking', 'MainController@booking');
Route::post('send-mail', 'MainController@sendMail');

Route::any('admin/{all?}', 'AdminController@index')->where('all', '.*');

Route::get('/api/get-photo/{id}', 'AdminController@getPhoto');
Route::get('/api/get-photos', 'AdminController@getPhotos');
Route::post('/api/delete-photo/{id}', 'AdminController@deletePhoto');
Route::post('/api/upload-photo', 'AdminController@uploadPhoto');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('blockedAppointmentHours', 'BlockedAppointmentHourController');

Route::post('/blockedAppointmentHours/deleteByDate', 'BlockedAppointmentHourController@deleteByDate');