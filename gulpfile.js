const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss')
       .webpack('firebase-config.js')
       .webpack('app.js')      
       .webpack('gallery-paginator.js')  //vue.js files are individually loaded (no imports), so we minify them
       .webpack('paginator.js')
       .webpack('about-us.js')
       .webpack('homepage-accordion.js')
       .webpack('react/calendar-app.js') //uses imports so don't need to call others
       .webpack('react/admin/src/index.js')
        // .webpack('react/app.js', './public/js/react/app.js')                                   
       .styles([ //all included in all.css (or app.css?)
           'animate.css',
           'fontello.css',
           'react-calendar.css',
           'front-end.css'
       ])
       .version([
           'js/app.js',
           'js/gallery-paginator.js',
           'js/paginator.js',
           'js/about-us.js',
           'js/homepage-accordion.js',
           'js/calendar-app.js',
           'css/all.css',
           'css/app.css',           
       ])       
       .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/','public/build/fonts/bootstrap');
}); 
