@extends('layouts.main')

@section('title', 'Services')

@section('content')    
    <style>
        .services-list{
            padding: 0;                        
        }

        .services-list li{
            list-style: none;
            padding: 15px 0;
            background-color: #000;
            color: #fff;
            font-weight: bold;
            max-width: 90%;
            padding-left: 30px;
            text-transform: uppercase;
            margin-bottom: 3px;
        }

        .services-list li.current{
            background-color: #CB1021;
        }

        .lead-in{
            margin-left: 40px;
            font-size: 14px;
            color: #999;            
        }

        .section div{
            padding: 0;
        }        
    </style>

    <div class="row section">
        <div class="col-md-3 hidden-xs hidden-sm">
            <ul class="services-list">
                <li>                                        
                    Our Services
                </li>
            </ul>
            <div class="lead-in">
                We offer high quality services.
            </div> 
        </div>

        <div class="col-md-9 col-sm-12">
            <div id="services-paginator">
                <paginator :objects="objects" :per-row="perRow"></paginator>
            </div>
        </div>
    </div>

    <script src="{{ elixir('js/paginator.js') }}"></script>
@stop