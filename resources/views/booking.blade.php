@extends('layouts.main')

@section('title', 'Book With Us')

@section('content')
<style>

    .google-map iframe{
        width: 100%;
    }

    .contact-line{
        margin: 10px 0;
    }

    .contact-line > span{
        margin: 0 5px;
    }

    .contact-line span.glyphicon{
        color: #CB1021;
        float: left;
        margin-top: 4px;
        font-size: 30px;
        margin-right: 30px;
    }

    .section {
        height: 50%;
        font-size: 16px;
    }
</style>

<div class="row section">    
    <div class="col-xs-12">
        <h1>Give us a call</h1>
    </div>

    <div class="col-xs-12 col-sm-4 contact-line">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <div>Serving the Greater Sacramento, Woodland, Davis area</div>                
    </div>

    <div class="col-xs-12 col-sm-4 contact-line">
        <span class="glyphicon glyphicon glyphicon-earphone" aria-hidden="true"></span>
        <div>Phone: <a href="tel:+5303795405">530-379-5405</a></div>        
    </div>

    <div class="col-xs-12 col-sm-4 contact-line">
        <span class="glyphicon glyphicon glyphicon-envelope" aria-hidden="true"></span>        
        <div>Web: www.ivansmobiledetailing.com</div>
        <div>Email: ivansmobiledetailing@gmail.com</div>
    </div>

    <div class="col-xs-12">
    
    <h1>Or Book Online</h1>
    
    <p>
        You can use the calendar below to request an appointment with us. Click on any day in the calendar, then follow the steps
        to complete your request. We will give you a call once we receive your request to confirm your appointment.
    </p>
    <p>
        If you don't see a time or service that works for you, please give us a call directly and we'd be happy to assist!
    </p>
    </div>    

    <div id="app"></div>        
</div>
<script src="{{ elixir('js/calendar-app.js') }}"></script>
@stop