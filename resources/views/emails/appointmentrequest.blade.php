<p>
    A new appointment request has been made. Give this person a call to confirm or reschedule. Here are the details:
</p>

<div>Name: {{ $name }}</div>
<div>Phone: {{ $phoneNumber }}</div>
<div>Service: {{ $service }}</div>
<div>Date: {{ $date }}</div>
<div>Time: {{ $hour }}:{{ $minute }} {{ $ampm }}</div>
<div>Notes: {{ $notes }}</div>