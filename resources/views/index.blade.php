@section('title', 'Ivan\'s Mobile Detailing')

@extends('layouts.main')

@section('home-carousel')
    <style>
        .carousel-main{
            height: 60%;
        }

        .carousel-main img{            
            width: 100%;       
        }       

        @media (max-width: 768px) {
            .carousel-main{
                height: 45%;
            }                            
            
            #homepage-accordion {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }        
        } 

        .carousel-inner .item {
            /*max-height: 700px;*/
            height: 100%;
            width: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            background-position-y: 50%;            
        }

        .carousel-main .carousel-indicators li{
            background-color: #000;
            border: none;
            width: 16px;
            height: 16px;            
            margin: 0 5px;
        }

        .carousel-indicators .active{
            background-color: #CB1021 !important;
        }

        .carousel-inner .info-box{
            width: 520px;
            height: auto;         
            left: 249px;
            position: absolute;
            padding: 70px;
            background: rgba(0,0,0,0.8);
            margin-top: 5%;
        }

        .carousel-inner .info-box h2{
            text-transform: uppercase;
            color: #fff;
            font-size: 22px;
            font-weight: bold;
        }

        .carousel-inner .info-box h2 span{
            color: #CB1021;
        }

        .carousel-inner .info-box p{
            color: #999;
            margin-top: 30px;
        }

        @media (max-width: 980px){
            .info-box{
                display: none;
            }
        } 

        .accordion {
            margin-bottom: 0;
        }
    </style>

    <div class="row">
        <div id="homepage-accordion" class="col-xs-12 visible-xs">
            <accordion v-for="card in cards" :card="card" @click.native="expand(card, $event)"></accordion>            
        </div>      
    </div>              
    <script src="{{ elixir('js/homepage-accordion.js') }}"></script>                             
    
    <div class="row">
        <div class="carousel-main">     
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">            
                <div class="item active" style="background-image: url(/images/bmw.jpg);">              
                    <div class="info-box">
                        <h2>Welcome to <span>Ivan's Mobile Detailing</span></h2>
                        <p>
                            Ivan's Mobile Detailing is focused on providing high-quality service, customer satisfaction, and a deep desire to connect with each and every client.                        
                        </p>

                        <p>
                            We provide services that include Interior, Carpets, Wax, and More. We welcome Autos, Boats, RVs, Big Rigs, Auto Fleet Dealers, and More.                        
                        </p>                        
                    </div>               
                </div>
                <div class="item" style="background-image: url(/images/porsche.jpg);">
                    <div class="info-box">
                        <h2>Detailing with <span>Precision</span></h2>
                        <p>
                            With a keen eye and a sharp attention to detail, we will bring life back to your car.                        
                        </p>
                    </div>                                
                </div>
                
                <div class="item" style="background-image: url(/images/interior.jpg);">
                    <div class="info-box">
                        <h2>Excellent <span>High Quality Services</span></h2>
                        <p>
                            Take a look around our website and book a detailing or car wash appointment with us today!                        
                        </p>
                    </div>                                                 
                </div>

            </div>     
            </div>        
        </div>
    </div>
    
    <script>

        $(document).ready(function(){
            $('.info-box').css({
                left: $('.new-image-navbar').offset().left,
                marginTop: '5%'
            });
            
            $(window).resize(function(){

                $('.info-box').css({
                    left: $('.new-image-navbar').offset().left,
                    marginTop: '5%'
                });

            });
        });

    </script>
@stop

