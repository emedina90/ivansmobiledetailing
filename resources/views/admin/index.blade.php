<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ivan's Mobile Detailing - Admin</title>
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/all.css') }}" />

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-93352199-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script src="{{ elixir('js/app.js') }}"></script>
        <style>
            body {
                background-color: #efefef !important;
            }

            .app-body {
                margin: 0 5px;
            }

            .admin-navbar {
                padding-left: 10px;
                line-height: 50px;
                margin: 0;
                background-color: #CB1021;
                color: #fff;
                font-weight: bold;
                -webkit-box-shadow: rgba(0, 0, 0, 0.3) 0 1px 3px;
                -moz-box-shadow: rgba(0,0,0,0.3) 0 1px 3px;
                box-shadow: rgba(0, 0, 0, 0.3) 0 1px 3px;
            }

            .menu-item {
                padding: 20px 15px;
                border: 1px solid #fff;
                -webkit-box-shadow: rgba(0, 0, 0, 0.3) 0 1px 3px;
                -moz-box-shadow: rgba(0,0,0,0.3) 0 1px 3px;
                box-shadow: rgba(0, 0, 0, 0.3) 0 1px 3px;
                margin: 10px 0;
                border-radius: 5px;
                text-align: center;
                font-weight: bold;
                color: #fff;
                cursor: pointer;
            }

            .menu-item a,
            a.button {
                text-decoration: none;
                color: #fff;
            }

            .menu-item.one {
                background-color: #006699;
            }

            .btn.admin-go-back {
                margin: 10px 0 0 0;
                font-weight: bold;
            }

            h1 {
                text-align: center;
            }

            .photos-list-item img {
                max-width: 100%;
            }

            .photos-list-item {
                height: 200px;
                overflow: hidden;
            }

            @media (max-width: 467px) {
                .photos-list-item {
                    height: 135px;
                }
            }

            .img-responsive {
                max-width: 100%;
            }

            .btn-container {
                text-align: center;
                margin-top: 15px;
            }

            .btn.pull-right {
                right: 0;
                left: auto;
                position: absolute;
                margin: 15px;
            }

            .flash {
               margin-bottom: 0 !important;
               text-align: center;
            }

            .flash.success {
                background-color: green;
            }

            .flash.error {
                background-color: red;
            }

            .btn.home {
                position: absolute;
                right: 10px;
                top: 10px;
            }

            a.logout {
                float: right;
                margin: 0 5px;
            }

            a.logout:hover {
                cursor: pointer;
            }

            .blocked-hours-list li {
                color: white;
                list-style: none;
                float: left;
                border: 1px solid #dedede;
                border-radius: 25%;
                padding:10px;
                margin:5px;
                background-color: lightgreen;
                cursor: pointer;
            }

            .blocked-hour {
                background-color: lightcoral !important;
            }
        </style>
    </head>
    <body>
        <div id="app">
        </div>
        <script src="{{ elixir('js/index.js') }}"></script>
    </body>
</html>