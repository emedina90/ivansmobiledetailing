<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ivan's Mobile Detailing - @yield('title')</title>
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/all.css') }}" />
        <script src="https://www.gstatic.com/firebasejs/3.6.10/firebase.js"></script>
        <script src="{{ elixir('js/firebase-config.js') }}"></script>        
        <script src="{{ elixir('js/app.js') }}"></script>        

        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-93352199-1', 'auto');
        ga('send', 'pageview');

        </script>        
    </head>

    <body>
        <div class="row">
            <div class="container header-container">
                <div class="row">
                    <div class="header">
                        <div class="container">
                            <div class="row row-center-mobile">
                                <div class="logo-container">
                                    <a href="/">                
                                        <img src="/images/logo.jpg" />
                                    </a>
                                </div>

                                <div class="booking-options">
                                    <img src="/svg/opentime.svg" data-toggle="modal" data-target=".hours-modal" />   

                                    <div class="book-online-button" onclick="location.href='/booking'">
                                        Book Now!
                                    </div>             
                                </div>                
                            </div>
                        </div>                       
                    </div>

                    <div class="nav-container">        
                            <nav class="navbar navbar-default new-image-navbar">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>                        
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                            <li><a href="/">Home</a></li>
                                            <li><a href="/services">Services</a></li>                                                                
                                            <li><a href="/about-us">About Us</a></li>
                                            <li><a href="/booking">Book Now</a></li>                            
                                        </ul>            
                                    </div><!-- /.navbar-collapse -->
                                </div><!-- /.container-fluid -->
                            </nav>         
                    </div>        
                    
                    <div class="page-title">
                        <h1>@yield('header')</h1>
                    </div>
                </div>
            </div>
        </div>

        @yield('home-carousel')        
        <div class="row main">
            <div class="container">            
                @yield('content')            
            </div>
        </div>
        
        <div class="row">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 social-icons">
                            <a href="#"><i class="demo-icon icon-facebook"></i>Facebook</a>
                        </div>

                        <div class="col-xs-12 copyright">
                            Copyright © 2017 Ivan's Mobile Detailing
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Hours Modal -->
        <div class="modal fade hours-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header hours">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Hours</h4>
                    </div>

                    <div class="modal-body">
                        <div><span>Monday:</span><span>8.00 - 5.00</span></div>
                        <div><span>Tuesday:</span><span>8.00 - 5.00</span></div>
                        <div><span>Wednesday:</span><span>8.00 - 5.00</span></div>
                        <div><span>Thursday:</span><span>8.00 - 5.00</span></div>
                        <div><span>Friday:</span><span>8.00 - 5.00</span></div>
                        <div><span>Saturday:</span><span>8.00 - 5.00</span></div>
                        <div><span>Sunday:</span><span>Closed</span></div>
                        <div><span>Special Hours Welcome by Request</span></div>
                    </div>
                </div>
            </div>
        </div>        

    </body>
</html>