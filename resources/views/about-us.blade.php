@extends('layouts.main')

@section('title', 'About Us')

@section('content')
    <style>                
        #about-us-accordion{
            margin-top: 25px;
        }        

        .accordion-link{
            color: #fff;
            text-decoration: underline;
        }

        #gallery-paginator .panel-body{
            padding: 10px 0 0 0 !important;                        
        }

        .panel,
        .panel *{
            border-radius: 30px !important;
        }

        .panel-body .img-responsive{
            border-radius: 30px;
        }

        @media (min-width: 1199px){
            #gallery-paginator .panel-body{
                height: 431px;
            }

            #gallery-paginator .panel-body img{
                height: 405px;
            }
        }

        @media (max-width: 768px) {
            #about-us-accordion {
                margin-top: 0;
                padding-left: 0 !important;
                padding-right: 0 !important;

            }

            .row.section{
                padding-top: 0;
            }

            .mobile-padding{
                padding-left: 25px;
            }
        }
        
    </style>

    <div class="row section">
        <div id="about-us-accordion" class="col-xs-12 col-sm-6">
            <accordion v-for="card in cards" :card="card" @click.native="expand(card, $event)"></accordion>            
        </div>

        <div class="col-xs-12 col-sm-6 mobile-padding">
            <h3><b>Proudly Serving the Greater Woodland Area</b></h3>

            <p>
               Ivans Mobile Detailing is proudly serving the Davis, Sacramento, Woodland, and surrounding areas. We will gladly drive to your location and service
               your vehicle with great precision and care. Take a look at our gallery below.            
            </p>            
        </div>                    
    </div>

    <div class="row section">
        <div id="gallery-paginator">
            <paginator v-if="objects && objects.length" :objects="objects" :per-row="perRow"></paginator>
        </div>        
    </div>
    <script src="{{ elixir('js/gallery-paginator.js') }}"></script>                
    <script src="{{ elixir('js/about-us.js') }}"></script>            
@stop