const app = new Vue({
    el: '#services-paginator',
    
    components: {        
        'paginator': require('./components/ObjectPaginator.vue')
    },

    data: {
        perRow: 12,        
        objects: [
            {
                title: 'Full Service Wash',
                description: 'We come to you and deliver high quality detailing to your vehicles. Our Full Service Wash includes the following:',
                details: [
                    'Vacuum Seats, Floor and Mats',
                    'Inside and Outside Window Wash',
                    'Dust dashboard, center console, and door panels',
                    'Wash and hand dry exterior & door jams',
                    'Triple Coat Polish',
                    'Wheel Cleaning',
                    'Tire Dressing',
                    'Air Freshener'                    
                ],
                prices: [
                    {
                        price: '$60',
                        unit: 'per vehicle'
                    },
                    {
                        price: '$65',
                        unit: 'and up per lifted truck'
                    },
                    {
                        price: '$120',
                        unit: 'and up per RV'
                    },                                        
                ]                
            },
            {
                title: 'Exterior Detail',
                description: 'Price varies depending on vehicle type. Includes "Full Wash" plus the following:',
                details: [
                    'Protective Wax',
                    'Dress and Clean All Exterior Vinyl',
                    'Clean Exterior Chrome'             
                ],
                prices: [
                    {
                        prefix: 'Starts at',
                        price: '$200',
                        unit: 'per vehicle'
                    },                                                            
                ],
                class: 'exterior-header'
            },     
            {
                title: 'Interior Detail',
                description: 'Price varies depending on vehicle type. Includes "Full Wash" plus the following:',
                details: [
                    'Clean and Condition all Leather and Vinyl',
                    'Shampoo carpets, floor mats, upholstry',
                    'Clean all Door Jambs'             
                ],
                prices: [
                    {
                        prefix: 'Starts at',
                        price: '$200',
                        unit: 'per vehicle'
                    },                                                            
                ],
                class: 'interior-header'
            },                      
            {
                title: 'Complete Detail',
                description: 'Price varies depending on vehicle type. Includes "Full Wash" plus the following:',
                details: [
                    'Includes Interior and Exterior Detail',
                    'Trunk Detail Service',
                    'Paint Sealant'             
                ],
                prices: [
                    {
                        prefix: 'Starts at',
                        price: '$350',
                        unit: 'per vehicle'
                    },                                                            
                ],
                class: 'complete-header'
            },      
            {
                title: 'Ceramic Coating Packages',
                description: 'Price varies depending on vehicle type.',
                details: [    
                    '1 year, 3 year, and 5 year packages available'
                ],
                prices: [
                    {
                        prefix: 'Starts at',
                        price: '$800',
                        unit: 'per vehicle'
                    },                                                            
                ],
                class: 'ceramic-header'
            },                  
            {
                title: 'Additional Services',
                description: 'We also offer these additional add-on services:',
                details: [
                    'Headlight Restoration - $70 per pair',
                    'Engine Bay - $70',
                    'Full Detail for Motorcycles',
                ],
                prices: [],
                class: 'additional-header'
            },                                                 
        ]
    },

    methods: {        
    }    
});