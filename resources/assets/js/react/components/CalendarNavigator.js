import React from 'react';
import CalendarNavigationButton from './CalendarNavigationButton';

const CalendarNavigator = (props) => {
    const { month, year, handleClick } = props;
    var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    var displayMonth = monthNames[month];
    
    return (
        <div className="calendar-navigator col-xs-12" style={styles.calendarNavigator}>
            <span className="calendar-navigator-back"><CalendarNavigationButton direction="back" handleClick={(direction) => handleClick(direction)} /></span>
                {displayMonth} {year}
            <span className="calendar-navigator-forward"><CalendarNavigationButton direction="forward" handleClick={(direction) => handleClick(direction)} /></span>
        </div>
    );
};

const styles = {
    calendarNavigator: {
        backgroundColor: '#CB1021',
        color: '#fff',
        textTransform: 'uppercase',
        textAlign: 'center',
        fontWeight: 'bold'        
    }     
};

export default CalendarNavigator;