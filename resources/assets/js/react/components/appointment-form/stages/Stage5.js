import React from 'react';

const Stage5 = (props) => {
    const { notes, onChange, onClick, styles } = props;

    let div = (
        <div style={styles.dropdown} className="stage five">
            <div className="appointment-builder-header">
                <p>Thank you! You're request has been forwarded to us.</p>
                <p>We will be reaching out to you soon to confirm your request.</p>                            
            </div>             
        </div>
    );

    return (
        div         
    );

}

const styles = { 
};

export { Stage5 };