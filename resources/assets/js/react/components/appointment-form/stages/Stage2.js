import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

var hasAvailableTimeslots = true;

const Stage2 = (props) => {
    const { hour, minute, ampm, onClick, onChange, styles, appointments } = props;
    var slot, opHours = [];

    var availableTimeslots = {
        "8:00AM": true,
        "9:00AM": true,
        "10:00AM": true,
        "11:00AM": true,
        "12:00PM": true,
        "1:00PM": true,
        "2:00PM": true,
        "3:00PM": true,
        "4:00PM": true,
        "5:00PM": true        
    };

    if (null !== appointments) { //set available times
        for (var i in appointments) {
            if (appointments.hasOwnProperty(i)) {
                //build time string
                let hour = appointments[i].hour;
                let ampm = (hour >= 8 && hour < 12) ? 'AM' : 'PM';
                slot = [hour, ':', '00', ampm].join('');

                if (availableTimeslots[slot]) {
                    availableTimeslots[slot] = false;
                }
            }
        } 
    }

    for (var j in availableTimeslots) {
        if (availableTimeslots.hasOwnProperty(j)) {
            if (availableTimeslots[j]) {
                opHours.push(<option key={j} value={j}>{j}</option>);
                hasAvailableTimeslots = false;
            }
        }
    }

    console.log(opHours, opHours.length);

    if (opHours.length == 0) {
        opHours.push(<option key="no-hours-available" value="">This day is all booked or unavailable!</option>);
    }
            
    let div = (
        <div key={2} style={styles.dropdown} className="stage two">
            <div className="appointment-builder-header">Select a Time Slot We Have Available</div>
            <div className="form-group">
                <select onChange={onChange} value={[hour, ':', minute, ampm].join('')} className="appointment-hour form-control">
                    {opHours}
                </select>            
            </div>

            <div className="form-group">
                <button className="btn btn-sm btn-info btn-back" onClick={onClick.back}>Back</button>
                {renderNextButton(onClick)}
                <div className="clear"></div>                
            </div>     
        </div>
    );

    return (           
        div                
    );

}

function renderNextButton(onClick) {
    if (!hasAvailableTimeslots) {
        return <button className="btn btn-sm btn-primary btn-next" onClick={onClick.next}>Next</button>;
    }
}

const styles = { 
};

export { Stage2 };