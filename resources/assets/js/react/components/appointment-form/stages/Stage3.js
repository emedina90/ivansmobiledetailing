import React from 'react';

var errorMessages = {
    phoneNumber: 'A 10-digit phone number is required. Example: 123-456-7890'
}

var showError = false;

const validatePhoneNumber = (phoneNumber, next, updatePhonenumber) => {
    var pn = phoneNumber.replace(/[^0-9]/ig, '');
    showError = (10 > pn.length);    
    updatePhonenumber(phoneNumber);            
    if(!showError) { next(); }
}

const Stage3 = (props) => {
    const { notes, phoneNumber, onChange, onClick, styles, updatePhonenumber, name } = props;

    let div = (
        <div key={3} style={styles.dropdown} className="stage three">
            <div className="form-group">
                <div className="appointment-builder-header">Phone Number</div>
                <div className="form-error">{(showError) ? errorMessages.phoneNumber : null}</div>
                <input name="phoneNumber" className="form-control" value={phoneNumber} onChange={onChange} />
            </div>

            <div className="form-group">
                <div className="appointment-builder-header">What is your name?</div>                             
                <input value={name} onChange={onChange} name="name" className="form-control" />
            </div>            

            <div className="form-group">
                <div className="appointment-builder-header">What type of car(s) do you have? Anything else we should know?</div>                             
                <textarea value={notes} onChange={onChange} name="notes" className="form-control" />
            </div>
            <div className="form-group">
                <button className="btn btn-sm btn-info btn-back" onClick={onClick.back}>Back</button>
                <button className="btn btn-sm btn-primary btn-next" onClick={() => { validatePhoneNumber(phoneNumber, onClick.next, updatePhonenumber)} }>Next</button>
                <div className="clear"></div>            
            </div>
        </div>
    );

    return (
        div         
    );

};

const styles = { 
    
};

export { Stage3 };