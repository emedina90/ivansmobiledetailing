import React from 'react';
        
const Stage4 = (props) => {
    const { notes, onChange, onClick, styles, date, hour, minute, ampm, service, phoneNumber, name } = props;

    let div = (
        <div style={styles.dropdown} className="stage four">
            <div className="appointment-builder-header">
                <p>Please make sure the contact information you've given us is correct.</p>
                <p>If any information is incorrect, we will be unable to reach you to confirm your request.</p>
                <p>Click "Go Back" to update any previously given information, otherwise, click "Submit Request"</p>                            
            </div>

            <div className="form-group">
                <div className="summary"><b>Date:</b> {date}</div>
                <div className="summary"><b>Time:</b> {hour}:{minute} {ampm}</div>
                <div className="summary"><b>Phone Number:</b> {phoneNumber}</div>
                <div className="summary"><b>Name:</b> {name}</div>
                <div className="summary"><b>Service:</b> {service}</div>
                <div className="summary"><b>Notes:</b> {notes}</div>
            </div>

            <div className="form-group">
                <button className="btn btn-sm btn-info btn-back" onClick={onClick.back}>No, Go Back</button>
                <button className="btn btn-sm btn-primary btn-next" onClick={onClick.next}>Submit Request</button>
                <div className="clear"></div>            
            </div>                         
        </div>
    );

    return (
        div         
    );

}

const styles = { 
};

export { Stage4 };