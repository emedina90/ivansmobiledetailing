import React from 'react';

const services = [
    {id: 1, name: 'Full Service Wash'}, 
    {id: 2, name: 'Exterior Detail'},
    {id: 3, name: 'Interior Detail'},
    {id: 4, name: 'Complete Detail'}
 ];

const Stage1 = (props) => {   
    const {onClick, styles} = props;
    
    let li = services.map((service, idx) => 
        <li key={service.id} onClick={() => onClick(service.name)}>
            {service.name}
            <span className="raquo">&raquo;</span>
        </li>            
    );    

    let div = ( 
        <div key={1} style={styles.dropdown} className="stage one">            
            <div className="appointment-builder-header">Select a Service</div>
            <ul className="services-list">
                {li}
            </ul>
        </div>    
     );    

    return (
        div        
    );

}

const styles = {     
};

export { Stage1 };