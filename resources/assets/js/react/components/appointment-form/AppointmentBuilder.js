import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import * as Stages from './stages';

var stages = [];
var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
const BACKEND_EMAIL_ENDPOINT = '/send-mail';

class AppointmentBuilder extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {            
            currentStageIdx: 0,
            date: (monthNames[props.date.getMonth()] +' '+props.day+', '+props.date.getFullYear()),
            hour: '12', 
            minute: '00', 
            ampm: 'PM',
            service: '',
            name: '',
            phoneNumber: '',
            notes: '',
            appointments: null
        }

        this.ref                 = firebase.database().ref();        

        this.props               = props;
        this.handleServicesClick = this.handleServicesClick.bind(this);
        this.nextStage           = this.nextStage.bind(this);
        this.previousStage       = this.previousStage.bind(this);    
        this.handleChange        = this.handleChange.bind(this);
        this.handleTimeChange    = this.handleTimeChange.bind(this);
        this.submit              = this.submit.bind(this);
        this.dbSave              = this.dbSave.bind(this);
        this.updatePhonenumber   = this.updatePhonenumber.bind(this);    
        this.getAppointments     = this.getAppointments.bind(this);    
        this.sendEmail           = this.sendEmail.bind(this);
    }

    nextStage() {        
        let currentStageIdx = (this.state.currentStageIdx + 1);

        if (undefined === stages[currentStageIdx]){ return; }

        this.setState({
            currentStageIdx: currentStageIdx
        });
    }

    previousStage() {
        let currentStageIdx = (this.state.currentStageIdx - 1);

        if (undefined === stages[currentStageIdx]){ return; }

        this.setState({
            currentStageIdx: currentStageIdx
        });        
    }
    
    componentWillMount() {                        
        for(var i in Stages){            
            stages.push(Stages[i]);            
        }                        
    }

    componentDidMount() {
        this.getAppointments();
    }

    updatePhonenumber(pn) {
        this.setState({
            phoneNumber: pn
        });
    }

    handleChange(e) {
        const name = e.target.name;
        const val  = e.target.value;
        
        this.setState({
            [name]: val
        });
    }

    handleServicesClick(service) {        
        this.setState({
            service: service
        });

        this.nextStage();
    }

    handleTimeChange(e) {
        var value  = e.target.value; // 8:00AM
        var hour   = value.split(':')[0];
        var minute = value.split(':')[1].substr(0, 2);
        var ampm   = value.substr(-2);
        
        this.setState({
            hour: hour,
            minute: minute,
            ampm: ampm
        });        
    }

    getAppointments() { //for this day
        var self = this;

        $.get('/blockedAppointmentHours?date=' + this.getSelectedDate(), function(data) {
            self.setState({
                appointments: data
            });
            console.log(data);
        });
    }

    getDate(date) {
        var today = (date ? date : (new Date()));
        var day   = today.getDate();
        var month = today.getMonth() + 1;
        var year  = today.getFullYear();

        if (day < 10) {
            day = '0' + day;
        }

        if (month < 10) {
            month = '0' + month;
        }

        return year + '-' + month + '-' + day;
    }

    getSelectedDate() {
        var date = this.props.date;
        date.setDate(this.props.day);
        return this.getDate(date);
    }

    dbSave() {
        var newKey = this.ref.child('appointments').push().key;
        var schema = {};

        var self   = this;

        schema['appointments/'+newKey] = {
            ampm: this.state.ampm,
            date: this.state.date,
            hour: this.state.hour,
            minute: this.state.minute
        };

        schema['appointment_infos/'+newKey] = {
            notes: this.state.notes,
            phoneNumber: this.state.phoneNumber,
            service: this.state.service,
            name: this.state.name
        };

        $.post('/blockedAppointmentHours', {
            hour: parseInt(self.state.hour),
            date: self.getSelectedDate()
        });

        this.sendEmail(schema, newKey);
    }

    sendEmail(schema, newKey) { //this didnt work in a promise, for safari?    
        let body = Object.assign(schema['appointments/'+newKey], schema['appointment_infos/'+newKey]);                                  
        //fetch not working on safari and safari based webkits, webpack does not poly fill this? Gotta use jquery...            
        $.post(BACKEND_EMAIL_ENDPOINT, {data: body});
    }

    submit() {
        //store state in firebase
        this.dbSave();
        this.nextStage();
    }

    dynamicPropHandler(currentStage) {
        var dynamicProps = {};
        switch(currentStage){
            case 0: 
                dynamicProps.onClick = this.handleServicesClick;
                break;            
            case 1:                            
                dynamicProps          = this.state;
                dynamicProps.onClick  = { next: this.nextStage, back: this.previousStage };
                dynamicProps.onChange = this.handleTimeChange;
                break;
            case 2:
                // const { notes, onChange } = props;
                dynamicProps     = this.state;
                dynamicProps.onChange = this.handleChange;
                dynamicProps.updatePhonenumber = this.updatePhonenumber;
                dynamicProps.onClick  = { next: this.nextStage, back: this.previousStage };
                break;
            case 3:
                // const { notes, onChange } = props;
                dynamicProps     = this.state;                
                dynamicProps.onClick  = { next: this.submit, back: this.previousStage };
                break;                
        }

        return dynamicProps;
    }
                
    render() {   
        let Stage = stages[this.state.currentStageIdx];
        var dynamicProps = this.dynamicPropHandler(this.state.currentStageIdx);
                
        return (
            <ReactCSSTransitionGroup
                transitionName="slide-left"
                transitionAppear={false}
                transitionAppearTimeout={0}
                transitionEnter={true}
                transitionEnterTimeout={500}
                transitionLeave={true}    
                transitionLeaveTimeout={500}                                         
                component="span">                        
                    <Stage key={this.state.currentStageIdx} {...dynamicProps} styles={styles} />
            </ReactCSSTransitionGroup>            
        );                        
    }
}
const styles = {
    dropdown: {        
         backgroundColor: '#dedede',
         textAlign: 'center',                 
    },
};
export default AppointmentBuilder;