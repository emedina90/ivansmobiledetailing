import React from 'react';
import CalendarDayDropdown from './CalendarDayDropdown';

class CalendarDay extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            isExpanded: false
        }

        this.props = props;

        this.isExpandable = this.isExpandable.bind(this);
        this.expand       = this.expand.bind(this);
    }

    expand() {        
        this.setState({
            isExpanded: !this.state.isExpanded
        });
    }

    /*
    *  A day is only expandable if it is in the future
    *
    */    
    isExpandable() {        
        var currentDate  = new Date().setHours(0,0,0,0);
        var givenDate    = new Date(this.props.date.getFullYear(), this.props.date.getMonth(), this.props.day).setHours(0,0,0,0);
                                
        return (givenDate >= currentDate);        
    }
    
    render() {
        const { date, day } = this.props;
                                
        var out = [];
        if (this.state.isExpanded) {
            out.push(<CalendarDayDropdown key="calendar-day-dropdown" expand={this.expand} date={date} day={day} />);                       
        }

        out.push((
            <div className="day" 
                style={(this.isExpandable()) ? styles.day : styles.pastDay}
                onClick={(this.isExpandable()) ? this.expand : false}
                key="calendar-day"
            >
                {(this.props.day > 0) ? this.props.day : ''}
            </div>            
        ));

        return (
            <div>
                {out}
            </div>
        );
    }
}

const styles = {
    day: {
        width: '14.2%',
        border: '1px solid #dedede',
        float: 'left',
        height: '50px',
    },    
    expandedDay: {
        width: '100%',
        border: '1px solid #dedede',        
    },
    pastDay: {
        width: '14.2%',
        border: '1px solid #dedede',
        float: 'left',
        color: '#dedede',
        height: '50px',        
    }
};

export default CalendarDay;