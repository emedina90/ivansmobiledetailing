import React from 'react';
import CalendarNavigator from './CalendarNavigator';
import CalendarMonthTable from './CalendarMonthTable';

class Calendar extends React.Component {
    constructor(props) {
        super(props);
        let date = new Date();
        this.state = {
            month: date.getMonth(),
            day: date.getDate(),
            year: date.getFullYear()
        };        

        this.handleClick = this.handleClick.bind(this);
        this.updateDate = this.updateDate.bind(this);        
    }

    handleClick(direction) {        
        this.updateDate(direction);
    }

    updateDate(direction) {
        var newDate, newMonth, newDay, newYear;
        var { month, day,  year } = this.state;                

        switch(direction) {
            case 'back':                                       
                if(0 > (month - 1)) {
                    newYear  = year - 1;
                    newMonth = 11;                    
                } else {
                    newYear  = year;
                    newMonth = month - 1;
                }                                
                break;
            case 'forward':
                if(11 < (month + 1)) {
                    newYear  = year + 1;
                    newMonth = 0;                    
                } else {
                    newYear  = year;
                    newMonth = month + 1;
                }                                                            
                break;
            default:
                break;
        }
        
        this.setState({
            year: newYear,
            month: newMonth,
        });
    }
    
    render() {
        return (
            <div className="col-xs-12">
                <CalendarNavigator
                    month={this.state.month}
                    year={this.state.year}
                    handleClick={this.handleClick}                     
                />
                <CalendarMonthTable
                    date={new Date(this.state.year, this.state.month)} 
                />                
            </div>
        );
    }
}

export default Calendar;