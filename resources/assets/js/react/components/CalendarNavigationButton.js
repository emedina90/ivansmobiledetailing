import React from 'react';

const CalendarNavigationButton = (props) => {
    const { direction, handleClick } = props;
    return (
        <button className="btn btn-sm btn-default" onClick={() => handleClick(direction)}>{(('back' === direction) ? '«' : '»')}</button>
    );
};

export default CalendarNavigationButton;