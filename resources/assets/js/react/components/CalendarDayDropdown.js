import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import AppointmentBuilder from './appointment-form/AppointmentBuilder';

class CalendarDayDropdown extends React.Component { 
    constructor(props) {
        super(props);
        this.props = props;                
    }

    componentDidMount() {
        let body = document.querySelector('body');
        //add modal-open class to body to prevent scrolling
        body.classList.add('modal-open');  
    }    

    componentWillUnmount() {
        let body = document.querySelector('body');
        //add modal-open class to body to prevent scrolling
        body.classList.remove('modal-open');        
    }
        
    render() {
        var { expand }      = this.props;
        const { date, day } = this.props;

        return (
            <div>
                <div style={styles.overlayShadow} className="calendar-day-overlay-shadow"></div>
                <ReactCSSTransitionGroup
                        transitionName="slide-down"
                        transitionAppear={true}
                        transitionAppearTimeout={0}
                        transitionEnter={false}
                        transitionLeave={false}                                             
                        component="span">                
                            <div style={styles.overlay} className="calendar-day-overlay">
                                <div style={styles.dropdown} className="calendar-day-dropdown">                                                                
                                    <div className="calendar-day-dropdown-close" style={styles.closeButton} onClick={expand}><span style={styles.closeIcon}>&times;</span></div>
                                    <div className="calendar-day-dropdown-content" style={styles.content}><AppointmentBuilder date={date} day={day} /></div>
                                </div>                                                
                            </div>
                </ReactCSSTransitionGroup>                
            </div>                
        );                        
    }
}

const styles = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '1050',
        overflow: 'auto',    
    },
    dropdown: {
        position: 'relative',
        top: '15%',
        width: '100%',
        // backgroundColor: '#dedede',        
    },
    overlayShadow: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: '1040',
        backgroundColor: '#000',
        opacity: 0.4,      
    },
    closeButton: {        
        backgroundColor: '#CB1021',
        color: '#fff',
        textAlign: 'right',
        paddingRight: '15px',
        fontSize: '20px', 
    },
    closeIcon: {
        cursor: 'pointer'
    },   
    content: {
        
    }
};

export default CalendarDayDropdown;