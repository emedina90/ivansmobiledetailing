import React from 'react';
import CalendarMonthRow from './CalendarMonthRow';

const CalendarMonthTable = (props) => {
    const { date }     = props;
    const daysInMonth  = parseInt(new Date(date.getFullYear(), (date.getMonth() + 1), 0).getDate(), 10);
    const days         = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];             
    var rows           = [];

    const daysRow = days.map((day) =>
        <div key={day} style={styles.day}>{day}</div> 
    );    

    var start = 1;
    var end   = (7 - date.getDay());
    for (var i = 0; i < 5; i++) {
         var dayOfWeek = (0 === i) ? date.getDay() : 0;
         rows.push(<CalendarMonthRow start={dayOfWeek} days={[start, end]} key={i} date={date} />);
         start = end + 1;
         end   = Math.min((end + 7), daysInMonth); 
    }
          
    return (
        <div className="calendar-month-table col-xs-12">
            <div className="calendar-month-days col-xs-12">
                {daysRow}
            </div>            
            {rows}    
        </div>
    );
};

const styles = {
    day: {
        width: '14.2%',        
        float: 'left',        
        overflowX: 'hidden',
        textAlign: 'center'
    }    
};

export default CalendarMonthTable;