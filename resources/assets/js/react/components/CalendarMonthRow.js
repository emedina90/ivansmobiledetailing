import React from 'react';
import CalendarDay from './CalendarDay';

const CalendarMonthRow = (props) => {
    var { start, days, date } = props;    
    var row = [];
    var div;

    for(var i = days[0]; i <= days[1]; i++){
        row.push(<CalendarDay day={i} key={i} date={date} />);        
    }
        
    if (0 !== start) {
        let fillerCount = 7 - row.length;
        for(var j = 0; j < fillerCount; j++){
            row.unshift(<CalendarDay day={0} date={date} key={'filler' + j} />);            
        }                
    }

    return (
        <div className="col-xs-12 calendar-month-row">
            {row}
        </div>
    );

}

const styles = {
    day: {
        width: '14.2%',
        border: '1px solid #dedede',
        float: 'left',
        height: '50px',
    },    
};

export default CalendarMonthRow;