import { FLASH, CLEAR } from '../actions/index';

const INITIAL_STATE = { messages: [] };

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case FLASH:
            return { messages: [...state.messages, action.payload] };
        case CLEAR:
            let messages = state.messages.concat();
            messages.pop();            
            return { messages: messages };
        default:
            return state;
    }
}