import { combineReducers } from 'redux';
import { reducer as formReducer} from 'redux-form';

import PhotoReducer from './photo_reducer';
import FlashReducer from './flash_reducer';
import UserReducer from './user_reducer';

const rootReducer = combineReducers({
    photos: PhotoReducer,
    flash: FlashReducer,
    form:  formReducer,
    user: UserReducer
});

export default rootReducer;