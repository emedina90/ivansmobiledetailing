import { LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT, UNAUTHORIZED } from '../actions/index';

const INITIAL_STATE = { auth: false, values: null };

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case LOGIN_SUCCESS:
            return { auth: action.payload.auth, values: action.payload.user };
        case LOGIN_FAILURE:        
            return { auth: false, values: null };
        case UNAUTHORIZED:
        case LOGOUT:
            return { auth: false, values: null };            
        default:
            return state;
    }
}