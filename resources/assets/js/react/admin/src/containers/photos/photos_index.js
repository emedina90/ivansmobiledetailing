import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import BackButton from '../../components/back_button.js';
import { fetchPhotos } from '../../actions/index.js';

class PhotosIndex extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchPhotos();
    }

    renderPhotos() {
        if (!this.props.all) {
            return <div>Loading...</div>;
        }

        return this.props.all.map((photo, i) => 
            <li className="list-group-item photos-list-item col-xs-4" key={photo.id}>
                <Link to={"/admin/photo/"+photo.id}>
                    <img src={photo.src} />
                </Link>
            </li>
        );
    }

    render() {
        const { history } = this.props;

        return (
            <div>
                <BackButton history={history} />
                <Link to="/admin/photos/new" className="btn btn-primary pull-right">New Photo</Link>
                <h1>Photos</h1>
                <div className="col-xs-12 col-sm-8 col-sm-offset-2">
                    <ul className="photos-list list-group">
                        {this.renderPhotos()}
                    </ul>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        all: state.photos.all
    };
}

export default connect(mapStateToProps, { fetchPhotos })(PhotosIndex);
