import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form'; 
import axios from 'axios';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import BackButton from '../../components/back_button.js';
import { uploadPhoto, flash } from '../../actions/index';

class PhotosNew extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    } 

    componentWillMount() {
        
    }

    submit(e) {
        e.preventDefault();
        let formData = new FormData();     
        formData.append('photo', document.getElementById('photo').files[0]);        
        axios.post('/api/upload-photo', formData)
             .then((response) => {
                 // @todo: Dispatch an action that triggers the notification bar
                 if (response.data.success){
                    this.props.flash({type: 'success', message: response.data.success});
                 } else {
                    this.props.flash({type: 'danger', message: response.data.error});
                 }
             })
             .catch((error) => {
                 this.props.flash({type: 'danger', message: "Network Error."});
             });
        this.props.flash({type: 'warning', message: 'Uploading image... you may upload more photos.'});
        e.target.reset();             
        return false;
    }

    render() {
        const { history } = this.props;
        return (
            <div>
                <BackButton history={history} /> 
                <form onSubmit={this.submit} encType="multipart/form-data">
                    <h3>Upload New Photo</h3>
                    <div className="form-group">
                        <input id="photo" name="photo" type="file" className="form-control" />
                    </div>

                    <button type="submit" className="btn btn-primary">Upload</button>
                </form>                               
            </div>
        );
    }
}

export default connect(null, { uploadPhoto, flash })(PhotosNew);
