import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import BackButton from '../../components/back_button.js';
import { fetchPhoto, deletePhoto, flash, clearFlash } from '../../actions/index.js';

class PhotosShow extends Component {
    constructor(props) {
        super(props);
        this.deletePhoto = this.deletePhoto.bind(this);
    }

    componentWillMount() {        
        this.props.fetchPhoto(this.props.match.params.id);
    }

    deletePhoto() {
        this.props.deletePhoto(this.props.photo.id).then((data) => {  
            if (data.payload.data.success) {
                this.props.flash({ type: "success", message: data.payload.data.success });
            } else {
                this.props.flash({ type: "success", message: data.payload.data.error });                
            }        
            this.props.history.push('/admin/photos');
        }).catch((error) => {
            this.props.flash({ type: "danger", message: "Network error." });
        });
    }

    renderPhoto() {
        if (!this.props.photo) {
            return <div>Loading...</div>;
        }

        return (
            <div>
                <div className="col-xs-6 col-xs-offset-3 photo-show">
                    <img className="img-responsive" src={this.props.photo.src} />
                </div>

                <div className="col-xs-12 btn-container">
                    <button className="btn btn-danger" onClick={this.deletePhoto}>Delete Photo</button>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <BackButton history={this.props.history} />
                {this.renderPhoto()}
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        photo: state.photos.photo
    };
}

export default connect(mapStateToProps, { fetchPhoto, deletePhoto, flash, clearFlash })(PhotosShow);