import React, { Component } from 'react';
import { connect } from 'react-redux';

import TopBar from '../components/top_bar.js';
import AdminMenu from '../components/menu.js';

class AdminHome extends Component {
    render() {        
        return ( 
            <div>
                <AdminMenu />                
            </div>
        );
    }
}

export default connect()(AdminHome);