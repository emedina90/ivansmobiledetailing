import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import axios from 'axios';
import { flash, login, handleLogin, LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE } from '../actions/index';

import RenderField from '../components/render_field.js';

class AdminForm extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }

    login(e) {
        e.preventDefault();      

        const { flash, history, login, handleLogin } = this.props;
        let formData = new FormData(e.target);     

        login(formData).then(response => {
            if (response.payload.data && response.payload.data.auth) {
                handleLogin(LOGIN_SUCCESS, response.payload.data);
                flash({type: 'success', message: "You are logged in."});
                history.push('/admin');
            } else {
                handleLogin(LOGIN_FAILURE);
                flash({type: 'warning', message: "Invalid login."});
            }
        });

        return false;
    }

    render() {
        const { handleSubmit, pristine, reset, submitting } = this.props;
        return ( 
            <div>
                <form className="form-horizontal" onSubmit={this.login}>
                    <Field 
                        name="email"
                        type="text"
                        component={RenderField}
                        label="Email Address"
                    />

                    <Field 
                        name="password"
                        type="password"
                        component={RenderField}
                        label="Password"
                    />

                    <div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </div>                                        
                </form>
            </div>
        );
    }
}

const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'Email address is required.';
    }

    if (!values.password) {
        errors.password = 'Password is required';
    }

    return errors;
}

AdminForm = reduxForm({
    form: 'LoginForm',
    validate
})(AdminForm);



export default connect(null, { flash, login, handleLogin })(AdminForm);