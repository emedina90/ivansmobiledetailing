import React, { Component } from 'react';
import { connect } from 'react-redux';

import Flash from '../components/flash';
import { clearFlash } from '../actions/index';  

class FlashContainer extends Component {
    constructor(props) {
        super(props);
    }

    renderFlashes() {
        const { messages, clearFlash } = this.props;

        const flashList = messages.map((flash, i) => {           
            return <Flash flash={flash} key={i} onClick={clearFlash} />;
        });
        return flashList;
    }

    render() {        
        return ( 
            <div>
                { this.renderFlashes() }               
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        messages: state.flash.messages
    };
}

export default connect(mapStateToProps, { clearFlash })(FlashContainer);