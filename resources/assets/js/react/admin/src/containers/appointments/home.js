import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import BackButton from '../../components/back_button.js';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { history } = this.props;

        return (
            <div>
                <BackButton history={history} />
                <h1>Appointments</h1>
                <Link to="/admin/appointments/blocked-hours" className="button"><div className="menu-item one">Blocked Hours</div></Link>
            </div>
        );
    }
}


export default Home;
