import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { flash } from '../../actions/index';
import BackButton from '../../components/back_button.js';

class AppointmentBlockedHours extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedDate: '',
            blockedHours: []
        };

        this.fetchBlockedHours = this.fetchBlockedHours.bind(this);
        this.onChange          = this.onChange.bind(this);
        this.toggleHour        = this.toggleHour.bind(this);
        this.submit            = this.submit.bind(this);
    }

    fetchBlockedHours(date) {
        var self = this;
        $.get('/blockedAppointmentHours?date=' + date, function (data) {
            let hours = data.map((blockedHour) =>
                parseInt(blockedHour.hour)
            );

            self.setState({
                blockedHours: hours
            });
        });
    }

    toggleHour(hour) {
        let hourIndex    = this.state.blockedHours.indexOf(hour);
        let blockedHours = this.state.blockedHours.slice();

        if (hourIndex === -1) {
            blockedHours.push(hour);
        } else {
            blockedHours.splice(hourIndex, 1);
        }

        this.setState({
            blockedHours: blockedHours
        });
    }

    renderBlockedHours() {
        if (this.state.selectedDate == '') {
            return "Please select a date first.";
        }

        let hours = [8, 9, 10, 11, 12, 1, 2, 3, 4, 5];
        let self  = this;

        const list = hours.map((hour) => {
            var className = '';
            if (self.state.blockedHours.indexOf(hour) !== -1) {
               className = "blocked-hour";
            }
            return <li key={hour} className={className} onClick={() => self.toggleHour(hour)}>{hour}</li>
        });

        return (
            <div>
                <ul className="blocked-hours-list">
                    {list}
                </ul>

                <button className="btn btn-primary btn-sm" style={{margin: "0 auto", clear: "both", display: "block"}} onClick={this.submit}>Submit Blocked Hours</button>
            </div>
        );
    }

    submit() {
        console.log("submitting", this.state.blockedHours);
        var self = this;

        for (var i in self.state.blockedHours) {
            if (self.state.blockedHours.hasOwnProperty(i)) {
                var blockedHour = self.state.blockedHours[i];
                $.post('/blockedAppointmentHours', {
                    "hour": blockedHour,
                    "date": self.state.selectedDate
                });
            }
        }

        let diff = this.getHoursDiff(this.state.blockedHours);

        for (var j in diff) {
            if (diff.hasOwnProperty(j)) {
                $.post('/blockedAppointmentHours/deleteByDate', {
                    "hour": diff[j],
                    "date": self.state.selectedDate
                });
            }
        }

        this.props.flash({type: 'success', message: "Successfully updated blocked hours"});
    }

    getHoursDiff(selectedHours) {
        let diff = [];
        let hours = [8,9,10,11,12,1,2,3,4,5];

        for (var i in hours) {
            if (hours.hasOwnProperty(i)) {
                if (selectedHours.indexOf(hours[i]) === -1) {
                    diff.push(hours[i])
                }
            }
        }

        return diff;
    }

    onChange(e) {
        this.fetchBlockedHours(e.target.value);
        this.setState({
            selectedDate: e.target.value
        });
    }

    render() {
        const { history } = this.props;
        return (
            <div>
                <BackButton history={history} />

                <h3>Select the day you'd like to block hours</h3>
                <div className="form-group">
                    <input className="form-control" type="date" name="date" onChange={this.onChange} />
                </div>

                <div className="container">
                    {this.renderBlockedHours()}
                </div>
            </div>
        );
    }
}

export default connect(null, { flash })(AppointmentBlockedHours);
