import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { 
    BrowserRouter as Router,
} from 'react-router-dom';
import reducers from './reducers';
import getRoutes from './routes';
import promise from 'redux-promise';
import { persistStore, autoRehydrate } from 'redux-persist';

import TopBar from './components/top_bar.js';
import FlashContainer from './containers/flash_container';
import { logout } from './actions/index';
import auth from './middleware/auth';

const store = compose(applyMiddleware(promise, auth), autoRehydrate())(createStore)(reducers);

class AppProvider extends Component {
    constructor() {
        super();
        this.state = { rehydrated: false };
    }

    componentWillMount() {
        persistStore(store, {}, () => {
            this.setState({ rehydrated: true });
        });
    }

    render() {
        if (!this.state.rehydrated) {
            return <div>Loading...</div>
        }
        
        return (
            <Provider store={store}>                                
                <Router store={store}>
                    <div>
                        <TopBar logout={logout}/>
                        <FlashContainer />                
                        <div className="app-body">
                            {getRoutes(store)}
                        </div>
                    </div>
                </Router>                
            </Provider>
        );
    }
}

ReactDOM.render(
    <AppProvider />
, document.querySelector('#app'));