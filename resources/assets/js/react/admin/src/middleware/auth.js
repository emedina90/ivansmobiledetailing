import { UNAUTHORIZED } from '../actions/index';

const auth = store => next => action => {    
    if (action.error            && 
        action.payload          && 
        action.payload.response &&
        action.payload.response.status == 401) {
             // remove auth from store if server returns auth error             
            store.dispatch({
                type: UNAUTHORIZED                
            });
    }

    return next(action);    
}

export default auth;