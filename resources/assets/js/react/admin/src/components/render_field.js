import React from 'react';

const RenderField = ({input, label, type, meta: {touched, error, warning}}) => (
  <div>
    <label className="control-label">{label}</label>
    <div className="form-group">
      <input className="form-control" {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

export default RenderField;