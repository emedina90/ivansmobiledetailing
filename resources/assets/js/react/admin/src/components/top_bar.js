import React from 'react';
import { Link } from 'react-router-dom';

const TopBar = (props) => {
    return (
        <div className="navbar navbar-default admin-navbar">
            Welcome, Ivan             
            <Link to="/admin/" className="btn btn-sm btn-primary home">Home</Link>
        </div>
    );
}

export default TopBar;