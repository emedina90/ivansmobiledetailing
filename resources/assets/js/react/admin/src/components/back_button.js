import React from 'react';

const BackButton = (props) => {
    const { history } = props;
    
    return (
        <button className="btn btn-default admin-go-back" onClick={history.goBack}>Back</button>
    );
}

export default BackButton;