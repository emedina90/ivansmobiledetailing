import React from 'react';

const Flash = (props) => {
    const { flash, onClick } = props;
    return (
        <div className={"flash alert alert-" + flash.type } onClick={onClick}>
            <span className="close">&times;</span>
            {flash.message}
        </div>
    );
}

export default Flash;