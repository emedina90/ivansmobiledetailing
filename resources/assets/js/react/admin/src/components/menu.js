import React from 'react';
import { Link } from 'react-router-dom';

const AdminMenu = ({props}) => {
    return (
        <div>
            <Link className="button" to="/admin/photos"><div className="menu-item one">Photos</div></Link>
            <Link className="button" to="/admin/appointments"><div className="menu-item one">Appointments</div></Link>
        </div>
    );
}

export default AdminMenu;