import React from 'react';
import {
    Route,
    Redirect
} from 'react-router-dom';

const PrivateRoute = (props) => {
    const { component: Component, store, path, exact } = props;
    let { user } = store.getState();

    if (!user.auth) {
        return <Redirect to="/admin/login" />
    }

    return (
        <Route exact={exact} path={path} render={ (props) => <Component {...props} /> }
        />
    );
};

export default PrivateRoute;