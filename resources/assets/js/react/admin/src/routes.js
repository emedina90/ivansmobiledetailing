import React from 'react';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './components/private_route';
import AdminHome   from './containers/home.js';
import PhotosIndex from './containers/photos/photos_index.js';
import PhotosShow  from './containers/photos/photos_show.js';
import PhotosNew   from './containers/photos/photos_new.js';
import AdminLogin  from './containers/admin_login.js';
import AppointmentsHome from './containers/appointments/home.js';
import AppointmentsBlockedHours from './containers/appointments/blocked_hours.js';

const getRoutes = (store) => {    
    return (
        <Switch>
            <Route path="/admin/login" component={AdminLogin} />
            <PrivateRoute exact store={store} path="/admin" component={AdminHome} />
            <PrivateRoute exact store={store} path="/admin/photos" component={PhotosIndex} />
            <PrivateRoute exact store={store} path="/admin/photos/new" component={PhotosNew} />        
            <PrivateRoute store={store} path="/admin/photo/:id" component={PhotosShow} />

            <PrivateRoute exact store={store} path="/admin/appointments" component={AppointmentsHome} />
            <PrivateRoute exact store={store} path="/admin/appointments/blocked-hours" component={AppointmentsBlockedHours} />
        </Switch>    
    );
};

export default getRoutes;