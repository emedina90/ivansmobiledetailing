import axios from 'axios';

export const FETCH_PHOTOS = 'FETCH_PHOTOS';
export const FETCH_PHOTO  = 'FETCH_PHOTO';
export const DELETE_PHOTO = 'DELETE_PHOTO';
export const UPLOAD_PHOTO  = 'UPLOAD_PHOTO';
export const FLASH         = 'FLASH';
export const CLEAR         = 'CLEAR';
export const LOGIN         = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT        = 'LOGOUT';
export const UNAUTHORIZED  = 'UNAUTHORIZED';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export function fetchPhotos() {
    const request = axios.get('/api/get-photos');

    return {
        type:    FETCH_PHOTOS,
        payload: request
    };
}

export function fetchPhoto(id) {
    const request = axios.get('/api/get-photo/'+id);

    return {
        type:    FETCH_PHOTO,
        payload: request
    };
}

export function deletePhoto(id) {
    const request = axios.post('/api/delete-photo/'+id);

    return {
        type:    DELETE_PHOTO,
        payload: request
    };
}

export function uploadPhoto() {
    return {
        type: UPLOAD_PHOTO,
        payload: request
    };
}

export function flash(flash) {
    return {
        type: FLASH,
        payload: flash
    };
}

export function clearFlash() {
    return {
        type: CLEAR        
    };
}

export function login(formData) {
    const request = axios.post('/login', formData);
    
    return {
        type: LOGIN,
        payload: request
    };
}

export function handleLogin(type, user = null) {
    return {
        type: type,
        payload: user
    };
}

export function logout() {
    const request = axios.post('/logout');

    return {
        type: LOGOUT,
        payload: request
    };
}