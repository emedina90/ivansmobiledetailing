import axios from 'axios';

const app = new Vue({
    el: '#gallery-paginator',
    
    components: {        
        'paginator': require('./components/ObjectPaginator.vue')
    },

    created(){
        var request = axios.get('/api/get-photos').then((photos) => {
            for (var i = 0; i < photos.data.length; i++) {
                this.objects.push(
                    {
                        description: '<img class="img-responsive" src="'+photos.data[i].src+'" />'    
                    }    
                );
            }
        });
    },

    data: {      
        perRow: 4,  
        objects: []
    },

    methods: {        
    }    
});