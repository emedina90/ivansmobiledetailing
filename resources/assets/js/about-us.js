const app = new Vue({
    el: '#about-us-accordion',
    
    components: {        
        'accordion': require('./components/Accordion.vue')
    },

    data: {
        cards: [
            {id: 1, title: 'Contact', description: '<div>Reach us by phone: <a class="accordion-link" href="tel:+5303795405">530-379-5405</a> or <a class="accordion-link" href="/booking">Book Online</a</div>', active: true},            
        ]
    },

    methods: {
        expand(card, event) {                        
            if(card.active) return;

            var $target = $(event.target).next('.description');                                             
            $target.slideDown();
            $('.accordion .description').not($target).slideUp();
            card.active = true;

            //switch all other cards off
            for(var i = 0; i < this.cards.length; i++){                                
                let c = this.cards[i];
                if(c.id == card.id) continue;
                c.active = false;
            }                             
        }
    }    
});