const app = new Vue({
    el: '#homepage-accordion',
    
    components: {        
        'accordion': require('./components/Accordion.vue')
    },

    data: {
        cards: [
            {id: 1, title: 'Welcome to Ivan\'s Mobile Detailing & Wash', description: "<p>Ivan's Mobile Detailing is focused on providing high-quality service, customer satisfaction, and a deep desire to connect with each and every client.</p><p>We provide services that include Interior, Carpets, Wax, and More. We welcome Autos, Boats, RVs, Big Rigs, Auto Fleet Dealers, and More.</p>", active: true},            
            {id: 2, title: 'Detailing with Precision', description: "With a keen eye and a sharp attention to detail, we will bring life back to your car.", active: false},
            {id: 3, title: 'Excellent High Quality Services', description: "Take a look around our website and book a detailing or car wash appointment with us today! ", active: false}                        
        ]
    },

    methods: {
        expand(card, event) {                        
            if(card.active) return;

            var $target = $(event.target).next('.description');                                             
            $target.slideDown();
            $('.accordion .description').not($target).slideUp();
            card.active = true;

            //switch all other cards off
            for(var i = 0; i < this.cards.length; i++){                                
                let c = this.cards[i];
                if(c.id == card.id) continue;
                c.active = false;
            }                             
        }
    }    
});