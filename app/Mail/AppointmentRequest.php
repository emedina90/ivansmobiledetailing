<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentRequest extends Mailable
{
    use Queueable, SerializesModels;
   
    public $ampm, $date, $hour, $minute, $notes, $phoneNumber, $service, $name;    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($props = []){
        $this->ampm        = $props['ampm'];
        $this->date        = $props['date'];
        $this->hour        = $props['hour'];
        $this->minute      = $props['minute'];
        $this->notes       = $props['notes'];
        $this->phoneNumber = $props['phoneNumber'];
        $this->service     = $props['service'];
        $this->name        = $props['name'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mailgun@'.env('MAILGUN_DOMAIN'))
                    ->view('emails.appointmentrequest');
    }
}
