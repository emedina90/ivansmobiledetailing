<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetupPhotos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photos:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setups photos for new CMS usage.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('Starting photos setup.');
        $this->info('Getting all existing photos, and moving them to storage.');

        $public_photos = public_path().'/images/gallery/';
        $this->info($public_photos);

        $files        = scandir($public_photos);        
        $photoConfig = [];

        foreach ($files as $idx => $file) {
            if (preg_match('/\.jpg|jpeg|png|gif$/ui', $file)) {
                $this->info('Copying file: '.$file);
                if (!copy($public_photos.$file, storage_path().'/app/public/images/gallery/'.$file)) {
                    $this->info('Unable to copy file: '.$file);
                    continue;
                }

                $photoConfig[] = [
                    'id'  => $idx,
                    'src' => '/storage/images/gallery/'.$file
                ];
            }        
        }

        $this->info('Creating PhotoConfig file.');
        if (!file_put_contents(storage_path().'/app/PhotoConfig.json', json_encode($photoConfig))) {
            $this->info('Unable to create PhotoConfig.json');
        }
        $this->info('Done.');
    }
}
