<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateAdminUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the initial admin users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('Creating Eric\'s admin user');        
        $result = User::create([
            'name' => 'Eric',
            'email' => 'medina.eric90@gmail.com',
            'password' => bcrypt('medinaeric789'),
        ]);        

        if (!$result) {
            $this->info('Failed to create Eric\'s user.');
        }

        $result = User::create([
            'name' => 'Ivan',
            'email' => 'ivansmobiledetailing@gmail.com',
            'password' => bcrypt('ivanmbd789'),
        ]);            

        if (!$result) {
            $this->info('Failed to create Ivan\'s user.');
        }        
        $this->info('Done.');
    }
}
