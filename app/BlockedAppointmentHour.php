<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BlockedAppointmentHour
 * @package App
 */
class BlockedAppointmentHour extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'hour',
        'date'
    ];
}
