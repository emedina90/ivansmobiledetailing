<?php

namespace App\Http\Controllers;

use App\Mail\AppointmentRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    public function index(Request $request) {
        return view('index');
    }

    public function services(Request $request) {
        return view('services');
    }

    public function aboutus(Request $request) {
        return view('about-us');
    }

    public function booking(Request $request) {
        return view('booking');
    }

    public function sendMail(Request $request) {        
        header('Content-Type: application/json');        
        $json = $request->input('data');
        $success = false;
        $response = [];
                        
        if ($request->isMethod('post') && isset($json['phoneNumber']) && (strlen($json['phoneNumber']) >= 7)) {
            Mail::to(env('APPOINTMENT_NOTIFY_EMAIL'))->bcc(env('ADMIN_EMAIL'))->send(new AppointmentRequest($json));
            $success = true;            
        }

        if ($success) {
            $response['success'] = 1;
        } else {
            $response['error'] = 'An error was encountered.';
        }

        echo json_encode($response);

        exit;
    }    
}
