<?php

namespace App\Http\Controllers;

use App\BlockedAppointmentHour;
use App\Http\Requests\CreateBlockedAppointmentHour;
use App\Http\Requests\ListBlockedAppointmentHours;
use App\Http\Requests\UpdateBlockedAppointmentHour;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


class BlockedAppointmentHourController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListBlockedAppointmentHours $request)
    {
        $date = $request->input('date');

        $blockedAppointmentHours = BlockedAppointmentHour::query();

        if ($date) {
            $blockedAppointmentHours->where('date', $date);
        }

        $blockedAppointmentHours = $blockedAppointmentHours->get();

        return response()->json($blockedAppointmentHours->toArray());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlockedAppointmentHour $request)
    {
        return response()->json(BlockedAppointmentHour::create($request->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BlockedAppointmentHour $blockedAppointmentHour)
    {
        return response()->json($blockedAppointmentHour->toArray());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlockedAppointmentHour $request, BlockedAppointmentHour $blockedAppointmentHour)
    {
        $blockedAppointmentHour->fill($request->toArray());
        $blockedAppointmentHour->save();

        return response()->json($blockedAppointmentHour->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlockedAppointmentHour $blockedAppointmentHour)
    {
        $blockedAppointmentHour->delete();

        return response('', 204);
    }


    /**
     * @param CreateBlockedAppointmentHour $request
     * @return JsonResponse
     */
    public function deleteByDate(CreateBlockedAppointmentHour $request)
    {
        BlockedAppointmentHour::where('hour', $request->input('hour'))->where('date', $request->input('date'))->delete();
        return response('', 204);
    }
}
