<?php

namespace App\Http\Controllers;

use App\Mail\AppointmentRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'getPhotos']]);
    }

    //
    public function index(Request $request) {
        return view('admin.index');
    }    

    public function getPhotos(Request $request) {
        try {
            $photosConfig = file_get_contents(storage_path('app/PhotoConfig.json'));
        } catch (\Exception $e) {
            return new JsonResponse();
            exit;
        }

        if (!$photosConfig) {
            $photosConfig = json_encode([]);
        }

        header('Content-Type: application/json');
        echo $photosConfig;
        exit;
    }

    public function getPhoto(Request $request, $id) {
        header('Content-Type: application/json');
        $photosConfig = json_decode(file_get_contents(storage_path('app/PhotoConfig.json')), true);
        if (!$photosConfig) {
            echo json_encode(['error' => 'Unable to fetch photos.']);
            exit;
        } 

        foreach ($photosConfig as $photo) {
            if ($id == $photo['id']) {
                echo json_encode($photo);
                exit;
            }
        }

        echo json_encode(['error' => 'Photo not found.']);
    }

    public function deletePhoto(Request $request, $id) {
        header('Content-Type: application/json');
        $photosConfig = json_decode(file_get_contents(storage_path('app/PhotoConfig.json')), true);

        if (!$photosConfig) {
            echo json_encode(['error' => 'Unable to fetch photos.']);
            exit;
        } 

        $found = false;
        foreach ($photosConfig as $i => $photo) {
            if ($photo['id'] == $id) {
                $found = true;
                unset($photosConfig[$i]);
                break;
            }
        }

        if (!$found) {
            echo json_encode(['error' => 'Unable to fetch photo '.$id]);
            exit;
        }

        $photosConfig = json_encode(array_values($photosConfig));

        if (!$photosConfig) {
            echo json_encode(['error' => 'Unable to delete photo.']);
            exit;
        }

        if (!file_put_contents(storage_path('app/PhotoConfig.json') ,$photosConfig)) {
            echo json_encode(['error' => 'Unable to delete photo.']);
            exit;
        }
        echo json_encode(['success' => 'Photo deleted']);
        exit;
    }

    public function uploadPhoto(Request $request) {
        header('Content-Type: application/json');
        $file = $request->file('photo');
        // validate that this is an image
        $validator = Validator::make(['photo' => $file], [
            'photo' => 'mimes:jpeg,bmp,png,gif,svg,pdf'
        ]);        

        if ($file && $file->isValid() && $validator->passes()) {
            if ( $file->storePublicly('public/images/gallery') ) {
                //save in PhotoConfig as first photo
                $photosConfig = json_decode(file_get_contents(storage_path('app/PhotoConfig.json')), true);

                array_unshift($photosConfig, [
                    'id'  => md5($file->hashName().time()),
                    'src' => '/storage/images/gallery/'.$file->hashName()
                ]);

                file_put_contents(storage_path('app/PhotoConfig.json'), json_encode($photosConfig));

                echo json_encode(['success' => 'File uploaded.']);                
            }
        } else {
            echo json_encode(['error' => 'Please provide a valid image file.']);
        }
    }        
}
